#include <stdio.h>
void mostrar_vector(double*vector,char* nombre){
    int i=0;
    for(i=0;i<3;i++)
        printf("%s[%d] = %lf\n",nombre,i,vector[i]);
    printf("\n");
}

double * suma(double * vector1 , double * vector2){
    double* result_suma=(double*)malloc(sizeof(double)*3);
    int i=0;
    for(i=0;i<3;i++)
        result_suma[i]=vector1[i]+vector2[i];
    return result_suma;
}

double * resta(double * vector1 , double * vector2){
    double* result_resta=(double*)malloc(sizeof(double)*3);
    int i=0;
    for(i=0;i<3;i++)
        result_resta[i]=vector1[i]-vector2[i];
    return result_resta;
}
double * multiplicacion_escalar(double * vector1 , double escalar){
    double* result=(double*)malloc(sizeof(double)*3);
    int i=0;
    for(i=0;i<3;i++)
        result[i]=vector1[i]*escalar;
    return result;
}

double producto_punto(double* vector1, double* vector2){
    double result=0;
    int i=0;
    for(i=0;i<3;i++)
        result=result + vector1[i]*vector2[i];
    return result;
}

double modulo_vector(double* vector){
    double modulo=0;
    int i=0;
    for(i=0;i<3;i++)
        modulo=modulo + vector[i]*vector[i];
    return pow(modulo,0.5);
}

double* componente_paralela(double*vector , double*vector_ref){
    return multiplicacion_escalar( vector_ref,producto_punto(vector,vector_ref)/pow(modulo_vector(vector_ref),2.0) );
}

double* componente_perpendicular(double*vector , double*vector_ref){
    return resta( vector ,multiplicacion_escalar( vector_ref,producto_punto(vector,vector_ref)/pow(modulo_vector(vector_ref),2.0) ));
}
