import java.io.*;
import java.net.*;
import java.nio.file.*;

public class Client {

	public static final int LIBRE = -1;
	public static final int OCUPADO = -2;
	public static final int ENVIANDO = -3;
	public static final int FINALIZADO = -4;

	public static void execute (String params) {
		Process p;
		try {
			//new File(params).mkdir();
			//Path source = Paths.get(".\\main.exe");
		    //Path target = Paths.get(".\\"+params+"\\main.exe");
			//Files.copy(source, target);
			//p = Runtime.getRuntime().exec(new String[] {"cd", params});
			//p.waitFor();

			String[] aux = params.split(" ");
			String[] cmd = new String[aux.length + 1];
			cmd[0] = ".\\main.exe";
			for (int i =1; i < cmd.length; i ++) {
				cmd[i] = aux[i-1];
			}

	    	p = Runtime.getRuntime().exec(cmd);
	    	p.waitFor();
			StringBuffer sb = new StringBuffer();
	    	BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			
			
			String line = "";			
	    	while ((line = reader.readLine())!= null) {
	    		sb.append(line + "\n");
	    	}
	    	System.out.println(sb.toString());
    	} catch(Exception e){
    		e.printStackTrace();
    	}
	}

	public static void main (String args[]) {
	    
		/*if (args.length != 2) {
	        System.err.println( "Usage: java EchoClient <host name> <port number>");
            System.exit(1);
	    }
	    String hostName = args[0];
	    int portNumber = Integer.parseInt(args[i]);*/
	    
	    String hostName = "127.0.0.1";
	    int portNumber = 8189;
	    int currentState = LIBRE;

	    int NFILES = 100;

	    int currentFile = 0;

	    try { 
	    	Socket socket = new Socket(hostName, portNumber);
	    	PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
	    	BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	    	BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
		
			String fromServer;
			String toServer;
			String output;

			toServer = currentState+"";
			String params;
			int fileState =0 ;

			File f = null;
			BufferedReader bfile = null;
			String readLine = "";


			while ((fromServer = in.readLine()) != null){

				if(fromServer.equals("Hello!")){
					System.out.println("Hello from Server");
				}
				
				if(currentState == LIBRE) {
					System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
					toServer = currentState+"";
					out.println(toServer);
					if(fromServer.contains("Tarea")){
						params = fromServer.replace("Tarea: ", "");
						execute(params);
						//System.out.println(params);
						currentState = OCUPADO;
					}
				}
				else if(currentState == OCUPADO) {
					
					if (fromServer.contains("OK")){
						//System.out.println("OK");
						currentState = ENVIANDO;
					}
					toServer = currentState+"";
					out.println(toServer);
				}
				else if (currentState == ENVIANDO) {

					if (currentFile == NFILES) {
						currentFile = 0;
						currentState = FINALIZADO;
						toServer = currentState+"";
					} else {
						//System.out.println(currentFile);
						
						if (fileState == 0) {
							toServer = "NOMBRE: "+currentFile+"";
							f = new  File(currentFile+".dat");
							bfile = new BufferedReader(new FileReader(f));
							fileState = 1;
						}
						else  if (fileState == 1) {
							readLine = bfile.readLine();

							if (readLine == null) {
								toServer = "FIN";
								fileState = 0;
								currentFile++;
							} else {
								//System.out.println(readLine);
								toServer = "CONTENIDO: " +readLine;
							}
						}
						

					}
					out.println(toServer);
					//System.out.println("ENVIANDO ..."+currentState);
				}

				else if(currentState == FINALIZADO) {
					currentState = LIBRE;
					toServer = currentState+"";
				}
			}

			/*while((userInput = stdIn.readLine()) != null) {
				out.println(userInput);
				System.out.println("Servidor: "+ in.readLine());
			}*/

			while( (fromServer = in.readLine()) != null) {
				if (fromServer.equals("Hello")) {
					System.out.println("Tarea recibida");
					execute("12 12");
				}
				output = "Esperando otra tarea";
				out.println(output);
			}
		} catch (UnknownHostException e) { 
			System.err.println("Direccion desconocida: "+hostName);
			System.exit(1);
		} catch (IOException e) {
			System.err.println("No se pudo establecer la conexion a: "+hostName);
			System.exit(1);

		}	   	
	}
}
