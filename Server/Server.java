import java.net.*;
import java.io.*;
import java.util.ArrayList;

public class Server {
	
	public static ArrayList<ClientThread> clients;

	public static String getCurrentTask () {
		if (taskArray.size() != 0){
			String task = taskArray.remove(0);
			return task;
		}

		return "ola";
	}

	public static ArrayList<String> taskArray;
	public static void main (String args[]) {
		/*
		if (args.length != 1) {
            System.err.println("Usage: java EchoServer <port number>");
            System.exit(1);
        }
        int portNumber = Integer.parseInt(args[0]);
        */
        taskArray = new ArrayList<>();
        taskArray.add("1 2");
        taskArray.add("3 4");
        taskArray.add("5 6");
        taskArray.add("7 8");
        taskArray.add("9 10");
        taskArray.add("9 10");
        taskArray.add("9 10");
        taskArray.add("9 10");
        taskArray.add("9 10");
        taskArray.add("9 10");
        taskArray.add("9 10");

        clients = new ArrayList<>();
        ClientThread client;

        int k = 0;
        int portNumber = 8189;

        while(true) {
	    	try{
    	     	ServerSocket serverSocket = new ServerSocket(portNumber);
		        Socket clientSocket = serverSocket.accept();
		        String address = (clientSocket.getRemoteSocketAddress()).toString();
		        
		        client = new ClientThread(clientSocket, "as");
		        clients.add(client);
		        client.start();

			}
			catch(Exception e){

			}
		}
	}
}

class ClientThread extends Thread{
	Socket clientSocket;
	String address;
	String task;

	public static final int LIBRE = -1;
	public static final int OCUPADO = -2;
	public static final int ENVIANDO = -3;
	public static final int FINALIZADO = -4;

	public ClientThread (Socket socket, String task) {
		clientSocket = socket;
		this.task = task;
		address = socket.getRemoteSocketAddress().toString();
	}

	public void run () {
		try {
    	    PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

	    	BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));

			System.out.println("\n************************************");
			System.out.println("Conexion establecida con: "+clientSocket.getRemoteSocketAddress());
			System.out.println("************************************\n");

			/*String userInput;
			while((userInput = stdIn.readLine()) != null) {
				out.println(userInput);
				System.out.println("Cliente: "+ in.readLine());
			}*/
			out.println("Hello!");
			
			int k= 0 ;
			String fromClient, task="";
			int currentClientState = LIBRE;
			String name, content;

			File file = null;
			FileOutputStream fos = null;
			BufferedWriter bw = null;

        	while((fromClient = in.readLine()) != null) {

        		if(fromClient.equals(LIBRE+"")) {
					//System.out.println("Cliente "+ address+" Libre");
					//task = "12314324";
					if (currentClientState != OCUPADO){
						task = Server.getCurrentTask();
						//System.out.println(Server.getCurrentTask());
						new File(".\\"+task).mkdir();
					}
					out.println("Tarea: "+task);
					currentClientState = OCUPADO;
        		}
        		else if(fromClient.equals(OCUPADO+"")) {
					out.println("OK");
					currentClientState = ENVIANDO;
        		}
        		
        		if (currentClientState == ENVIANDO ) {
        			if(fromClient.equals(ENVIANDO+"")){
        				//System.out.println("Esperando respuesta del cliente");
        			}else {
        				
        				if (fromClient.contains("NOMBRE")){
        					//System.out.println("Archivo recibido : "+fromClient);
        					name = fromClient.replace("NOMBRE: ", "");

        					file = new File(".\\"+task+"\\"+name+".dat");
        					fos = new FileOutputStream(file);
        					bw= new BufferedWriter(new OutputStreamWriter(fos));
        				}

        				else if(fromClient.contains("CONTENIDO")) {
        					content = fromClient.replace("CONTENIDO: ", "");
							//System.out.println("Contenido recibido : "+fromClient);
							bw.write(content);
							bw.newLine();
        				}
        				else if(fromClient.contains("FIN")){
        					System.out.println("******************FIN DE ARCHIVO*****************************");
        					bw.close();
        				}

        			}
        			out.println("RECIBIENDO");
        		}
				else if(fromClient.equals(FINALIZADO+"")){
        			currentClientState = LIBRE;
        		}
        		k++;
        	}
	        
	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	}
}


